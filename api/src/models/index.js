const path = require('path');
const Sequelize = require('sequelize');

// Load our database configuration
const dbConfig = require('../config/database');

// Connect Sequelize to the database
const sequelize = new Sequelize(dbConfig.database, dbConfig.user,
  dbConfig.password, dbConfig);

// Load all of our model definitions
const models = {
  Post: sequelize.import(path.resolve(__dirname, 'post.js')),
  Comment: sequelize.import(path.resolve(__dirname, 'comment.js'))
};

// Set up associations
models.Post.hasMany(models.Comment, {
  onDelete: 'CASCADE',
  hooks: 'true'
});
models.Comment.belongsTo(models.Post);

// Export our model definitions
module.exports = models;
